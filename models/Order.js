const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
    userId: {
        type: String,
    },
    products: [{
        productId: {
            type: String,
        },
        productName: {
            type: String,
        },
        quantity: {
            type: Number,
        },
        price: {
            type: Number,
        },
    }, ],

    totalAmount: {
        type: Number,
    },

    purchasedOn: {
        type: Date,
        default: Date.now,
    },
});

module.exports = mongoose.model("Order", orderSchema);