const bcrypt = require("bcrypt");
const User = require("../models/User");
const auth = require("../auth");
const Product = require("../models/Product");

module.exports.registerUser = (reqBody) => {

    let newUser = new User({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        password: bcrypt.hashSync(reqBody.password, 10)
    });

    return newUser.save()
        .then(user => user ? true : false)
        .catch(err => err);
};


module.exports.loginUser = (reqBody) => {
    return User.findOne({
            email: reqBody.email
        })
        .then(result => {
            if (!result) return false;
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
            return isPasswordCorrect ? {
                access: auth.createAccessToken(result)
            } : false;
        })
        .catch(err => err);
};


module.exports.createOrder = async (data, body) => {
    let isUserUpdated = await User.findById(data.userId).then(user => {
        user.products.push({
            productId: data.productId,
            quantity: data.quantity
        });

        return user.save().then(user => true).catch(err => err);
    });
    let isProductUpdated = await Product.findById(data.productId).then(product => {
        product.userOrders.push({
            userId: data.userId,
            orderId: data.orderId
        });
        console.log(product);
        return product.save().then(product => true).catch(err => err);
    });
    if (isUserUpdated && isProductUpdated) {
        return true;
    } else {
        return false;
    }
};

module.exports.getProfile = (data) => {

    return User.findById(data.userId).then(result => {
        return result;

    });

};