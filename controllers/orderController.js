const User = require("../models/User");
const auth = require("../auth");
const Product = require("../models/Product");
const Order = require("../models/Order");

module.exports.createOrder = async (data) => {
    let orderData = {
        userId: data.userId,
        products: [],
        totalAmount: data.totalAmount
    }

    for (const item of data.products) {
        const dataItem = await Product.findById(item.productId);
        if (!dataItem.isActive) {
            return false
        }
        orderData.products.push(item);
    }

    let newOrder = new Order(orderData);

    return newOrder.save()
        .then(order => order ? true : false)
        .catch(err => err);
};