const express = require("express");
const router = express.Router();
const auth = require("../auth");

const userController = require("../controllers/userController");


router.post("/register", (req, res) => {
    userController.registerUser(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err))
});

router.post("/login", (req, res) => {
    userController.loginUser(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err))
});


router.post("/createOrder", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);

    let data = {
        userId: userData.id,
        isAdmin: userData.isAdmin,
        productId: req.body.productId,
        quantity: req.body.quantity
    }

    if (!data.isAdmin) {
        userController.createOrder(data).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
    } else {
        res.send(false);
    }
});

// router.post("/checkout", auth.verify, (req, res) => {
//     const userData = auth.decode(req.headers.authorization);

//     let data = {
//       userId: userData.id,
//       isAdmin: userData.isAdmin,
//     };

//     if (!data.isAdmin) {
//       userController
//         .checkOut(data, req.body)
//         .then((resultFromController) => res.send(resultFromController))
//         .catch((err) => res.send(err));

//       console.log(req.body);
//     } else {
//       res.send(false);
//     }
//   });



router.get("/details", (req, res) => {
    const userData = auth.decode(req.headers.authorization);

    userController.getProfile({
        userId: userData.id
    }).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
});

module.exports = router;