const express = require("express");
const router = express.Router();

const orderController = require("../controllers/orderController");
const auth = require("../auth");


router.post("/createOrder", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);
    // console.log(req.body);
    let data = {
        userId: userData.id,
        products: req.body.products,
        totalAmount: req.body.totalAmount
    }

    if (!userData.isAdmin) {
        orderController.createOrder(data).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
    } else {
        res.send(false);
    }
});

module.exports = router;